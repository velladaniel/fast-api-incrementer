# FastAPI Incrementer

## Python Environment

Set up your preferred Python environment. For this project, I created an environment using the `venv` module.

```
python -m venv venv
source venv/bin/activate
```

## Installing Packages

Install the libraries required by install directly from the requirements.txt file. Should this be problematic due to certain dependencies, manually run the following command `pip install fastapi "uvicorn[standard]"`. 

```
pip install -r requirements.txt
```

## Running the app

To run the app, run the following command. Uvicorn should already be installed from the previous step.

```
uvicorn main:app --reload
```

## App Documentation

The application is developed using FastAPI. This simple application keeps track and returns an increment integer value. Updates to this value are allowed, and any updates made to this increment value are tracked by date, time and the incremented value.

All documentation about the applicaton can be accessed from the API documentation endpoint using Swagger UI from the `/docs` endpoint.
