from fastapi import FastAPI
from datetime import datetime
from pydantic import BaseModel
import asyncio

description = """
Implement a small HTTP server, serving 2 API calls. The server framework has to be either based on Gevent or Python's asyncio concurrency model, and can be in house or something readily available such as AIOHTTP or FastAPI.

## The API

* `POST` /increment with the following JSON as the request body; {"increment":value[int]}. The backend will maintain a global variable `increment_global` that will be incremented by this `increment` value. It also needs to keep a track of how many times this `increment_global` value was updated - `increment_updates`.

* `GET` /increment returns a JSON containing both the `increment_global` and `increment_updates` values.

## Bonus

* API protocol documentation such as OpenAPI specification

## Deliverable

* Provide a working application
* Provide source code in any public git such as GitHub or BitBucket
* Provide a markdown readme file with general information about the app and steps how to launch/build your application
"""

app = FastAPI(
    title="Backend Assessment - Daniel Vella",
    description=description,
    version="1.0.0",
    contact={
        "name": "Daniel Vella",
        "email": "danielvella3@gmail.com",
    },
)

class IncrementValue(BaseModel):
    increment: int

increment_global = 0
increment_updates = {}

lock = asyncio.Lock()

@app.get("/increment")
async def increment():
    """
        Returns a JSON containing both the `increment_global` and `increment_updates` values. The `increment_values` is returned as a count of the updates. This can be easily changed to return a dictionary with all the individual updates by time and value.
    """
    return {"increment_global": increment_global, "increment_updates": len(increment_updates)}


@app.post("/increment")
async def increment(increment: IncrementValue):
    """
        Post a JSON request body {"increment": value[int]}. The method adds the `increment` value to the global variable `increment_global`. The time and value of this operation is saved to the `increment_updates` variable.
    """
    global increment_global
    global increment_updates
    
    async with lock:
        dt_now = datetime.now()
        increment_global += increment.increment
        increment_updates[dt_now] = increment.increment

    return {"increment_global": increment_global, "increment_updates": len(increment_updates)}
